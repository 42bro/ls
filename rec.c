#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include "libft/libft.h"
#include "ls.h"

void	ft_putulong(size_t n)
{
	if (n <= 9)
		ft_putchar(n + '0');
	else
	{
		ft_putnbr(n / 10);
		ft_putnbr(n % 10);
	}
}

void	update_path(char **p)
{
	int i;

	i = 0;
	if (!p || (*p)[0] == 0)
		return ;
	//printf("PATH IS %s \n", *p);
	while ((*p)[i])
		i++;
	while (i && (*p)[i] != '/')
		i--;
	while (--i >= 0)
	{
		if ((*p)[i] == '/')
		{
			(*p)[++i] = 0;
			//			printf("PATH IS %s NOW \n", (*p));
			return ;
		}
	}
	if (i <= 0)
		(*p)[0] = 0;
	//printf("PATH IS %s NOW \n", (*p));
}

void	le_seum(DIR *d, char **p, t_env *e)
{
	struct dirent *dent;
	struct stat s;
	DIR	*dir;
	char filepath[MAX_PATH_SIZE * 2] = {0};
	t_li	*l;
	t_li *tmp;
	l = NULL;
	//	printf("initial path > %s \n", *p);
	while ((dent = readdir(d)) != NULL)
	{
		tmp = l;
		ft_strcpy(filepath, *p);
		ft_strcat(filepath, dent->d_name);
		if (dent->d_type == DT_LNK)
		{
			if (lstat(filepath, &s))
				perror("lstat says no :");
		}
		else
		{
			if (stat(filepath, &s))
				perror("stat says no :");
		}
		if (dent->d_type == DT_DIR && e->opt & OPT_BIGR && S_ISDIR(s.st_mode) && ft_strcmp(dent->d_name, ".") && ft_strcmp(dent->d_name, ".."))
		{
			ft_strcat(*p, dent->d_name);
			ft_strcat(*p, "/");
			if (!(dir = opendir(*p)))
			{
				perror(*p);
				update_path(p);
			}
			else
				le_seum(dir, p, e);
		}
		//	printf("filepath > %s | p > %s \n", filepath, *p);
		//	printf("dir %s FIRST IS %s ", *p, (l ? l->d->d_name : NULL));fflush(stdout);
		if (e->opt & OPT_R)
		{
			if (e->opt & OPT_L && e->opt & OPT_T && e->opt & OPT_U)
				add_node_rev(&l, new_node(dent, s), sort_atime);
			else if (e->opt & OPT_T)
				add_node_rev(&l, new_node(dent, s), sort_mtime);
			//	else if (e->opt & OPT_L & e->opt & OPT_C)
			//		add_node_rev(&l, new_node(dent, s), sort_ascii);
			else if (e->opt & OPT_C && !(e->opt & OPT_L))
				add_node_rev(&l, new_node(dent, s), sort_ctime);
			else if (e->opt & OPT_BIGU)
				add_node_rev(&l, new_node(dent, s), NULL);
			else
				add_node_rev(&l, new_node(dent, s), sort_ascii);
		}
		else
		{
			if (e->opt & OPT_L && e->opt & OPT_T && e->opt & OPT_U)
				add_node(&l, new_node(dent, s), sort_atime);
			else if (e->opt & OPT_T)
				add_node(&l, new_node(dent, s), sort_mtime);
			else if (e->opt & OPT_L & e->opt & OPT_C)
				add_node(&l, new_node(dent, s), sort_ascii);
			else if (e->opt & OPT_C && !(e->opt & OPT_L))
				add_node(&l, new_node(dent, s), sort_ctime);
			else if (e->opt & OPT_BIGU)
				add_node(&l, new_node(dent, s), NULL);
			else
				add_node(&l, new_node(dent, s), sort_ascii);
		}
		tmp = l;
		while (tmp)
		{
		//	printf("%s \n", tmp->d->d_name);fflush(stdout);
			tmp = tmp->next;
		}
	}
	if (e->opt & OPT_BIGR)
		display_path(*p);
	if (e->opt & OPT_L)
		l_display(l, e->opt, *p);
	else
		simple_display(l, e->opt);
	free_list(&l);
	if (d && closedir(d))
		perror("closedir says no :");
	update_path(p);
	if (ft_strncmp(*p, "\0", 1))
		write(1, "\n", 1);
}
