CC = gcc

FLAGS = -Wall -Wextra -Werror -g -fsanitize=address

DSRC = src/

DINC = include/

SRC = handlers.c display.c display_utils.c display_utils2.c ls.c rec.c list.c sorting.c sorting_utils.c

OBJ = $(SRC:%.c=%.o)

INC = ls.h

NAME = ft_ls

.PHONY: all clean fclean re

all: $(NAME)

$(OBJ): %.o : %.c $(INC)
	$(CC) $(FLAGS) -c $< -o $@

$(NAME): $(OBJ)
	$(CC) $(FLAGS) $^ -o $(NAME) -L libft/ -lft

clean:
	rm -rf $(OBJ)

fclean: clean
	rm -f $(NAME)

re: fclean all
