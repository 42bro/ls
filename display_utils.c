#include "ls.h"

size_t	digits(size_t nb)
{
	int	i;

	i = 1;
	if (!nb)
		return (i);
	while ((nb / 10))
	{
		nb /= 10;
		i++;
	}
	return (i);
}

size_t		max_grp_len(t_li *li)
{
	t_li	*tmp;
	size_t	max;
	struct group	*grp;

	max = 0;
	tmp = li;
	while (tmp)
	{
		if (!(grp = getgrgid(tmp->s.st_gid)))
			perror("getgrgid says no:");
		if (ft_strlen(grp->gr_name) > max)
			max = ft_strlen(grp->gr_name);
		tmp = tmp->next;
	}
	return (max);
}

size_t		max_pwd_len(t_li *li)
{
	t_li	*tmp;
	size_t	max;
	struct passwd	*pwd;

	max = 0;
	tmp = li;
	while (tmp)
	{
		if (!(pwd = getpwuid(tmp->s.st_uid)))
			perror("getpwuid says no:");
		if (ft_strlen(pwd->pw_name) > max)
			max = ft_strlen(pwd->pw_name);
		tmp = tmp->next;
	}
	return (max);
}

size_t		max_digits(t_li *li)
{
	t_li	*tmp;
	size_t	max;

	max = 0;
	tmp = li;
	while (tmp)
	{
		if (digits(tmp->s.st_size) > max)
			max = digits(tmp->s.st_size);
		tmp = tmp->next;
	}
	return (max);
}

size_t		max_link_digits(t_li *li)
{
	t_li	*tmp;
	size_t	max;

	max = 0;
	tmp = li;
	while (tmp)
	{
		if (digits(tmp->s.st_nlink) > max)
			max = digits(tmp->s.st_nlink);
		tmp = tmp->next;
	}
	return (max);
}
