/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putendl_fd.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fdubois <fdubois@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/12 17:06:27 by fdubois           #+#    #+#             */
/*   Updated: 2018/10/09 14:08:35 by fffelix          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	ft_putendl_fd(char const *s, int fd)
{
	if (s != NULL)
	{
		size_t i;

		i = 0;
		while (s[i] != '\0')
		{
			write(fd, s + i, 1);
			i++;
		}
		write(fd, "\n", 1);
	}
}
