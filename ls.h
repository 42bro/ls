#ifndef FT_LS_H
# define FT_LS_H

# include <dirent.h>
# include <stdlib.h>
# include <unistd.h>
# include <stdint.h>
# include <sys/types.h>
# include <sys/stat.h>
# include <sys/xattr.h>
# include <stdio.h>
# include <time.h>
# include "libft/libft.h"
# include <pwd.h>
# include <grp.h>

# define OPT_CHARS		"alrRthuicGgfUA"
# define OPT_A			1
# define OPT_L			(1 << 1)
# define OPT_R			(1 << 2)
# define OPT_BIGR		(1 << 3)
# define OPT_T			(1 << 4)
# define OPT_H			(1 << 5)
# define OPT_U			(1 << 6)
# define OPT_I			(1 << 7)
# define OPT_C			(1 << 8)
# define OPT_BIGG		(1 << 9)
# define OPT_G			(1 << 10)
# define OPT_F			(1 << 11)
# define OPT_BIGU		(1 << 12)
# define OPT_BIGA		(1 << 13)

# define MAX_PATH_SIZE	512
# define SPACES			"                                                "

typedef struct	s_li
{
	struct dirent	*d;
	struct s_li		*next;
	struct stat		s;
}				t_li;

typedef struct	s_env
{
	char		**paths;
	uint16_t	npaths;
	uint16_t	opt;
}				t_env;

t_li	*new_node(struct dirent *d, struct stat s);
void	add_node(t_li **list, t_li *node, int (*f)(t_li*, t_li*));
void	add_node_rev(t_li **list, t_li *node, int (*f)(t_li*, t_li*));
void	free_node(t_li **node);
int		list_size(t_li *list);
void	free_list(t_li **list);
void	free_mem(t_env **e);
void	gros_fail(t_env **e);

size_t	digits(size_t nb);
void	ft_putulong(size_t n);
size_t	max_grp_len(t_li *li);
size_t	max_pwd_len(t_li *li);
size_t	max_digits(t_li *li);
size_t	max_link_digits(t_li *li);
size_t	max_ino_digits(t_li *li);

void	sort_it(t_li **li, uint16_t o);
int		sort_mtime(t_li *a, t_li *b);
int		sort_atime(t_li *a, t_li *b);
int		sort_ctime(t_li *a, t_li *b);
int		sort_size(t_li *a, t_li *b);
int		sort_ascii(t_li *a, t_li *b);

int		to_display(t_li *tmp, uint16_t o);
void	display_path(char *str);
void	display_time(struct tm *t);
void	display_total(t_li *li, uint16_t o);
void	display_ino(t_li *li);
void	cheumod(mode_t mode);
void	color_it(t_li *node);
void	simple_display(t_li *li, uint16_t o);
void	le_seum(DIR *d, char **p, t_env *e);
void	l_display(t_li *li, uint16_t o, char *path);

#endif
