#include "ls.h"

size_t		max_ino_digits(t_li *li)
{
	t_li	*tmp;
	size_t	max;

	max = 0;
	tmp = li;
	while (tmp)
	{
		if (digits(tmp->s.st_ino) > max)
			max = digits(tmp->s.st_ino);
		tmp = tmp->next;
	}
	return (max);
}

void	display_path(char *str)
{
	if (!str || !str[0])
		write(1, ".", 1);
	else
		write(1, str, ft_strlen(str) - 1);
	write(1, ":\n", 2);
}

void	display_time(struct tm *t)
{
	char	*str;
	int		i;
	size_t	e;

	str = ctime((const time_t*)t);
	e = (size_t)time(NULL) - 15778800;
	i = 0;
	while (!ft_isdigit(str[i]))
		i++;
	while (ft_isdigit(str[i]))
		i++;
	if (e < (*(size_t*)t)) // young
	{
		while (str[i] != ':')
			i++;
		i += 2;
	}
	str[++i] = ' ';
	str[++i] = '\0';
	ft_putstr(ft_strchr(str, ' '));
	str[i++] = ' ';
	str = ft_strchr(str + i, ' ') + 1;
	if (e > (*(size_t*)t)) // young
	{
		write(1, str, 4);
		write(1, " ", 1);
	}
}

void	display_ino(t_li *li)
{
	ft_putulong(*(size_t*)&(li->s.st_ino));
	write(1, " ", 1);
}

void	display_total(t_li *li, uint16_t o)
{
	t_li	*tmp;
	int i;
	i = 0;
	tmp = li;
	while (tmp)
	{
		if (to_display(tmp, o))
			i += (tmp->s.st_blksize / 1024) * (tmp->s.st_blocks / sizeof(long int));
		tmp = tmp->next;
	}
	ft_putstr("total ");
	ft_putnbr(i);
	if (o & OPT_H)
		write(1, "K", 1);
	write(1, "\n", 1);
}

int		to_display(t_li *tmp, uint16_t o)
{
	return (o & OPT_A || tmp->d->d_name[0] != '.' || (o & OPT_BIGA
	&& ft_strcmp(tmp->d->d_name, ".") && ft_strcmp(tmp->d->d_name, "..")));
}
