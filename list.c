#include "ls.h"

t_li	*new_node(struct dirent *d, struct stat s)
{
	t_li	*node;

	if (!(node = (t_li*)malloc(sizeof(t_li))))
		return (NULL);
	node->s = s;
	node->d = d;
	node->next = NULL;
	return (node);
}

void	add_node(t_li **list, t_li *node, int (*f)(t_li*, t_li*))
{
	t_li	*tmp;

	tmp = *list;
	if (!node)
		return ;
	if (!(*list))
		*list = node;
	else if (!f)
	{
		while (tmp->next)
			tmp = tmp->next;
		tmp->next = node;
	}
	else
	{	
		if (f(node, tmp) > 0)
		{
			node->next = *list;
			*list = node;
		}
		else
		{
			while (tmp->next && f(node, tmp->next) < 0)
				tmp = tmp->next;
			while (tmp->next && !f(node, tmp->next) && sort_ascii(node, tmp->next) < 0)
				tmp = tmp->next;
			node->next = tmp->next;
			tmp->next = node;
		}
	}
	//printf("ADDED %s \n", node->d->d_name);
}

void	add_node_rev(t_li **list, t_li *node, int (*f)(t_li*, t_li*))
{
	t_li	*tmp;

	tmp = *list;
	if (!node)
		return ;
	if (!(*list))
		*list = node;
	else if (!f)
	{
		while (tmp->next)
			tmp = tmp->next;
		tmp->next = node;
	}
	else
	{	
		if (f(node, tmp) < 0)
		{
			node->next = *list;
			*list = node;
		}
		else
		{
			while (tmp->next && f(node, tmp->next) > 0)
				tmp = tmp->next;
			while (tmp->next && !f(node, tmp->next) && sort_ascii(node, tmp->next) > 0)
				tmp = tmp->next;
			node->next = tmp->next;
			tmp->next = node;
		}
	}
}

void	free_node(t_li **node)
{
	(*node)->next = NULL;
	ft_memdel((void**)node);
}

int		list_size(t_li *list)
{
	int	i;

	i = 0;
	if (list)
	{
		while (list)
		{
			list = list->next;
			i++;
		}
	}
	return (i);
}

void	free_list(t_li **list)
{
	if ((*list))
	{
		free_list(&((*list)->next));
		free_node(list);
	}
}
