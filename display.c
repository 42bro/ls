#include "ls.h"

void	ft_swap(void *a, void *b)
{
	void	*tmp;

	tmp = a;
	a = b;
	b = tmp;
}

void	simple_display(t_li *li, uint16_t o)
{
	t_li	*tmp;

	//	sort_it(&li, o);
	tmp = li;
	while (tmp)
	{
		if (to_display(tmp, o))
		{
			color_it(tmp);
			ft_putstr(tmp->d->d_name);
			write(1, "\033[0m", 4);
		}
		write(1, " ", 1);
		tmp = tmp->next;
	}
	write(1, "\n", 1);
}

int		get_size_unit(size_t size)
{
	int	i;

	i = 0;
	while ((size / 1024) && ++i)
		size /= 1024;
	return (i);
}

void	human_readable_size(size_t size)
{
	int		i;
	double	r;

	i = get_size_unit(size);
//	printf("unit %d \n", i);
	ft_putchar(' ');
	if (size < 1024)
	{
		write(1, SPACES, 3 - digits(size) + 1);
		ft_putnbr(size);
	}
	else
	{
		r = ((double)(size % (1 << (10 * i)))) / (1 << (10 * i)) * 10.0f;
		size = size >> (i * 10);
	//	write(1, SPACES, 3 - digits(size) + 1);
		ft_putnbr(size);
		if (i)
		{
			ft_putchar('.');
			ft_putchar('0' + (int)r);
			if (i > 2)
				write(1, (i == 2 ? "T" : "G"), 1);
			else
				write(1, (i == 2 ? "M" : "K"), 1);
		}
	}
}

void	color_it(t_li *node)
{
	if (S_ISCHR(node->s.st_mode) || S_ISBLK(node->s.st_mode))
		write(2, "\033[1m\033[33;40m", 12);
	else if (S_ISUID & node->s.st_mode || S_ISGID & node->s.st_mode)
		write(2, "\033[39;41m", 8);
	else if (S_ISLNK(node->s.st_mode))
		write(2, "\033[1m\033[96;40m", 12);
	else if (S_ISDIR(node->s.st_mode))
		write(2, "\033[1m\033[94;40m", 12);
	else if (S_IXUSR & node->s.st_mode || S_IXGRP & node->s.st_mode || S_IXOTH & node->s.st_mode)
		write(2, "\033[1m\033[92;40m", 12);
}

int		display_link(t_li *node, char *path)
{
	char *str;
	static char filepath[MAX_PATH_SIZE] = {0};
	int	size;

	size = (!node->s.st_size ? MAX_PATH_SIZE : node->s.st_size);
	if (!(str = ft_strnew(size)))
		return (0);
	ft_strcpy(filepath, path);
	ft_strcat(filepath, node->d->d_name);
//	ft_putstr(filepath);
	readlink(filepath, str, size);
	ft_putstr(" -> ");
	ft_putstr(str);
	ft_strdel(&str);
	return (1);
}

void	l_display(t_li *li, uint16_t o, char *path)
{
	t_li *tmp;
	struct passwd	*pwd;
	struct group	*grp;
	int	p;
	int g;
	int i;
	p = max_pwd_len(li);
	g = max_grp_len(li);
	i = max_ino_digits(li);
	tmp = li;
	display_total(li, o);
	while (tmp)
	{
		if (to_display(tmp, o))
		{
			//		printf("%s ", tmp->d->d_name);fflush(stdout);
			if (o & OPT_I)
			{
				write(1, SPACES, i - digits(tmp->s.st_ino));
				display_ino(tmp);
			}
			if (!(pwd = getpwuid(tmp->s.st_uid)))
				perror("getpwuid says no:");
			if (!(grp = getgrgid(tmp->s.st_gid)))
				perror("getgrgid says no:");
			//	printf("%s ", tmp->d->d_name);fflush(stdout);
			cheumod(tmp->s.st_mode);
			write(1, SPACES, (max_link_digits(li) - digits(tmp->s.st_nlink) + 1));
			ft_putnbr(tmp->s.st_nlink);
			//		write(1, " ", 1);
			if (!(o & OPT_G))
			{
				write(1, SPACES, (p - ft_strlen(pwd->pw_name) + 1));
				ft_putstr(pwd->pw_name);
			}
			if (!(o & OPT_BIGG))
			{
				write(1, SPACES, (g - ft_strlen(grp->gr_name) + 1));
				ft_putstr(grp->gr_name);
			}
			if (o & OPT_H)
				human_readable_size(tmp->s.st_size);
			else
			{
				write(1, SPACES, (max_digits(li) - digits(tmp->s.st_size) + 1));
				ft_putulong(tmp->s.st_size);
			}
			if (o & OPT_C)
				display_time((struct tm*)(&(tmp->s.st_ctime)));
			else if (o & OPT_U)
				display_time((struct tm*)(&(tmp->s.st_atime)));
			else
				display_time((struct tm*)(&(tmp->s.st_mtime)));
			if (!(o & OPT_F))
				color_it(tmp);
			ft_putstr(tmp->d->d_name);
			write(2, "\033[0m", 4);
			if (S_ISLNK(tmp->s.st_mode))
				display_link(tmp, path);
			ft_putchar('\n');
			//			if (S_ISDIR(tmp->s.st_mode) || S_IXUSR & tmp->s.st_mode || S_IXGRP & tmp->s.st_mode || S_IXOTH & tmp->s.st_mode || S_ISLNK(tmp->s.st_mode))
		}
		//	printf("%s ", tmp->d->d_name);fflush(stdout);
		tmp = tmp->next;
	}
}

