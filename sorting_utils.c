/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sorting_utils.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fdubois <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/29 09:34:24 by fdubois           #+#    #+#             */
/*   Updated: 2019/05/29 09:53:08 by fdubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ls.h"

int		sort_mtime(t_li *a, t_li *b)
{
	if ((*(size_t*)(&(a->s.st_mtime)) < *(size_t*)(&(b->s.st_mtime))))
		return (-1);
	else
		return ((*(size_t*)(&(a->s.st_mtime)) > *(size_t*)(&(b->s.st_mtime))));
}

int		sort_atime(t_li *a, t_li *b)
{
	if ((*(size_t*)(&(a->s.st_atime)) < *(size_t*)(&(b->s.st_atime))))
		return (-1);
	else
		return ((*(size_t*)(&(a->s.st_atime)) > *(size_t*)(&(b->s.st_atime))));
}

int		sort_ctime(t_li *a, t_li *b)
{
	if ((*(size_t*)(&(a->s.st_ctime)) < *(size_t*)(&(b->s.st_ctime))))
		return (-1);
	else
		return ((*(size_t*)(&(a->s.st_ctime)) > *(size_t*)(&(b->s.st_ctime))));
}

int		sort_size(t_li *a, t_li *b)
{
	if ((*(size_t*)(&(a->s.st_size)) < *(size_t*)(&(b->s.st_size))))
		return (-1);
	else
		return ((*(size_t*)(&(a->s.st_size)) > *(size_t*)(&(b->s.st_size))));
}

int	ft_isupper(char c)
{
	return (c < 91 && c > 64);
}

int	dotdot(char *str)
{
	return (!ft_strcmp(str, ".") || !ft_strcmp(str, ".."));
}

int ft_strcmplow(char *a, char *b)
{
	int tg;

	tg = 0;

	if (ft_isupper(a[0]))
	{
		a[0] += 32;
		tg = ft_strcmp(a, b);
		a[0] -= 32;
		return (tg);
	}
	else if (ft_isupper(b[0]))
	{
		b[0] += 32;
		tg = ft_strcmp(a, b);
		b[0] -= 32;
		return (tg);
	}
	else
		return (ft_strcmp(a, b));
}

int		sort_ascii(t_li *a, t_li *b)
{

	if (dotdot(a->d->d_name))
		return (1);
	else if (dotdot(b->d->d_name))
		return (-1);
	else if (!(ft_isalnum(a->d->d_name[0])) && !(ft_isalnum(b->d->d_name[0])))
		return (ft_strcmplow(&(*(b->d->d_name + 1)),&(*(a->d->d_name + 1))));
	else if (!(ft_isalnum(a->d->d_name[0])))
		return (ft_strcmplow(b->d->d_name, &(*(a->d->d_name + 1))));
	else if (!(ft_isalnum(b->d->d_name[0])))
		return (ft_strcmplow(&(*(b->d->d_name + 1)), a->d->d_name));
	else
		return (ft_strcmplow(b->d->d_name, a->d->d_name));
}
