#include "ls.h"

void	free_mem(t_env **e)
{
	int i;

	i = -1;
	while (++i < (*e)->npaths)
		if (&(*e)->paths[i])
			ft_memdel((void**)&((*e)->paths[i]));
	ft_memdel((void**)&((*e)->paths[i]));
	if ((*e)->paths)
		ft_memdel((void*)&((*e)->paths));
	if ((*e))
		ft_memdel((void**)e);
}

void	gros_fail(t_env **e)
{
	perror("nique ta race :");
	free_mem(e);
	exit(1);
}

void	cheumod(mode_t mode)
{
	char	str[11];

	str[10] = 0;
	if (S_ISFIFO(mode) || S_ISLNK(mode))
		str[0] = (S_ISLNK(mode) ? 'l' : 'p');
	else if (S_ISBLK(mode) || S_ISCHR(mode))
		str[0] = (S_ISCHR(mode) ? 'c' : 'b');
	else
		str[0] = (S_ISDIR(mode) ? 'd' : '-');
	str[1] = (mode & S_IRUSR ? 'r' : '-');
	str[2] = (mode & S_IWUSR ? 'w' : '-');
	if (mode & S_ISUID)
		str[3] = (mode & S_IXUSR ? 's' : 'S');
	else
		str[3] = (mode & S_IXUSR ? 'x' : '-');
	str[4] = (mode & S_IRGRP ? 'r' : '-');
	str[5] = (mode & S_IWGRP ? 'w' : '-');
	if (mode & S_ISGID)
		str[6] = (mode & S_IXGRP ? 's' : 'S');
	else
		str[6] = (mode & S_IXGRP ? 'x' : '-');
	str[7] = (mode & S_IROTH ? 'r' : '-');
	str[8] = (mode & S_IWOTH ? 'w' : '-');
	if (S_ISVTX & mode)
		str[9] = (mode & S_IXOTH ? 't' : 'T');
	else
		str[9] = (mode & S_IXOTH ? 'x' : '-');
//	str[10] = ' ';
	ft_putstr(str);
}

void	toggle_opt(t_env *e, char c)
{
	if (c == 'a')
		e->opt |= OPT_A;
	else if (c == 'l')
		e->opt |= OPT_L;
	else if (c == 'r')
		e->opt |= OPT_R;
	else if (c == 'R')
		e->opt |= OPT_BIGR;
	else if (c == 't')
		e->opt |= OPT_T;
	else if (c == 'c')
		e->opt |= OPT_C;
	else if (c == 'i')
		e->opt |= OPT_I;
	else if (c == 'u')
		e->opt |= OPT_U;
	else if (c == 'h')
		e->opt |= OPT_H;
	else if (c == 'G')
		e->opt |= OPT_BIGG;
	else if (c == 'g')
		e->opt |= OPT_G;
	else if (c == 'f')
		e->opt |= OPT_F;
	else if (c == 'U')
		e->opt |= OPT_BIGU;
	else if (c == 'A')
		e->opt |= OPT_BIGA;
}

void	invalid_opt(t_env *e, char c)
{
	ft_putstr("ls: invalid option -- '");
	if (ft_isprint(c))
		write(1, &c, 1);
	write(1, "'\n", 2);
	ft_putendl("Try 'git gud' for more information.");
	gros_fail(&e);
}

void	options_shenanigans(t_env *e)
{
	if (e->opt & OPT_F && !(e->opt & OPT_A))
		e->opt |= OPT_A;
	if (e->opt & OPT_F && !(e->opt & OPT_BIGU))
		e->opt |= OPT_BIGU;
	if (e->opt & OPT_F && e->opt & OPT_L)
		e->opt &= ~OPT_L;
	
}

int	options(t_env *e, int ac, char **av)
{
	int	i;
	int	j;

	i = 0;
	while (++i < ac && av[i][0] == '-')
	{
		j = 0;
		if (av[i][1] == '-')
		{
			i++;
			break ;
		}
		while (av[i][++j])
		{
			if (ft_strchr(OPT_CHARS, av[i][j]))
				toggle_opt(e, av[i][j]);
			else
				invalid_opt(e, av[i][j]);
		}
	}
	options_shenanigans(e);
	return (i);
}

void	do_the_ls_thing(t_env *e)
{
	int i;
	DIR				*d;

	i = -1;
	while (e->paths[++i][0])
	{
		if (!(d = opendir(e->paths[i])))
			perror("opendir says no :");
		else
			le_seum(d, &(e->paths[i]), e);
	}
}

void	add_slash(char **path)
{
	int	i;

	i = -1;
	while ((*path)[++i])
		;
	if (i - 1 && i < MAX_PATH_SIZE - 1 && (*path)[i - 1] != '/')
	{
		(*path)[i] = '/';
		(*path)[i + 1] = '\0';
	}
}

int main(int ac, char **av)
{
	t_env			*e;
	int i;
	int j;

	j = -1;
	//CAS RELOUS ICI
	if (!(e = (t_env*)malloc(sizeof(t_env))))
		gros_fail(NULL);
	ft_bzero(e, sizeof(t_env));
	i = options(e, ac, av);
	e->npaths = ac - i;
	(!e->npaths ? ++e->npaths : 1);
//	printf("%d i %d npaths \n", i, e->npaths);
	if (!(e->paths = (char**)malloc(sizeof(char*) * (e->npaths + 1))))
		gros_fail(&e);
	if (i == ac)
	{
		if(!(e->paths[++j] = ft_strnew(MAX_PATH_SIZE)))
			gros_fail(&e);
		ft_strcpy(e->paths[j], "./");
	}
	while (i < ac)
	{
		if(!(e->paths[++j] = ft_strnew(MAX_PATH_SIZE)))
			gros_fail(&e);
		ft_strcpy(e->paths[j], (!av[i][0] ? "./" : av[i]));
		if (av[i][0])
			add_slash(&(e->paths[j]));
//		printf(e->paths[j]);fflush(stdout);
		i++;
	}
	if (!(e->paths[++j] = ft_strnew(0)))
		gros_fail(&e);
	do_the_ls_thing(e);
	free_mem(&e);
	return (0);
}
